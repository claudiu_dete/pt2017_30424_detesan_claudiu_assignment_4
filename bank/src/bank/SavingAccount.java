package bank;

import java.util.Date;

public class SavingAccount extends Account {
	
	private final double rate=0.05;
	private boolean okStore;
	private boolean okWithdraw;
	
	
	public SavingAccount(int id,double value,Date c)
	{
		super(id,value,c);
		okStore=false;
		okWithdraw=false;
	}
	
	public double computeInterest()
	{
		return value*rate;
	}
	
	public void withdrawMoney(double sum)
	{
		if(okWithdraw==false)
		{
			setChanged();
			notifyObservers("The sum "+this.getValue()+" has been taken from account id="+this.getId());
			this.value=0;
			okWithdraw=true;
			
		}
	}
	
	public void storeMoney(double sum)
	{
		if(okStore==false)
		{
			setChanged();
			notifyObservers("The sum "+sum+" has been deposited in account id="+this.getId());
			this.value=value+sum+computeInterest();
			okStore=true;
		}
	}
	
	
	

}
