package bank;

import java.awt.CardLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import javax.swing.JComboBox;
import javax.swing.JRadioButton;
import java.awt.Font;
import javax.swing.SwingConstants;





public class GUI {
	
	private JFrame frame;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
	private JPanel panelClient;
	private JPanel panelProd;
	private JPanel panelStart;
	private JButton btnClient;
	private JButton btnProduct;
	private JButton btnReturn;
	private JButton btnInsert;
	private JButton btnDelete;
	private JButton btnUpdate;
	private JButton btnInsertProd;
	private JButton btnUpdateProd;
	private JButton btnDeleteProd;
	private JButton btnFind_1;
	private JButton btnShowProducts;
	private JButton btnStore;
	private JButton btnWithdraw;
	public JTable table;
	private JTextField textField_7;
	private JTable table_1;
	private JComboBox comboBox;
	private JButton btnSaveData;
	private static Bank b=new Bank();
	private JTextField textField_5;
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		   
		       
	           try {
	        	   FileInputStream inputFileStream = new FileInputStream("E:\\Facultate\\bank.ser");
				   ObjectInputStream objectInputStream = new ObjectInputStream(inputFileStream);
				   b.persons=(ArrayList<Person>)objectInputStream.readObject();
				   b.accounts=(HashMap<Person, ArrayList<Account>>) objectInputStream.readObject();
				   objectInputStream.close();
				   inputFileStream.close();
			}     catch (Exception e) {
				
				e.printStackTrace();
			}
			
	                 
					GUI window = new GUI();
					window.frame.setVisible(true);
				
		
	}

	/**
	 * Create the application.
	 */
	public GUI() {
		;
		
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		
		frame = new JFrame();
		frame.setBounds(100, 100, 575, 399);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new CardLayout(0, 0));
		
	    JPanel panelStart = new JPanel();
		frame.getContentPane().add(panelStart, "name_586332008121322");
		panelStart.setLayout(null);
		panelStart.setVisible(true);
		
		 JPanel panelClient = new JPanel();
			frame.getContentPane().add(panelClient, "name_586335308900019");
			panelClient.setLayout(null);
			panelClient.setVisible(false);
			
			 JPanel panelProd = new JPanel();
				frame.getContentPane().add(panelProd, "name_586337080291867");
				panelProd.setLayout(null);
				panelProd.setVisible(false);
		
		JButton btnClient = new JButton("Persons");
		btnClient.setFont(new Font("Tahoma", Font.PLAIN, 22));
		btnClient.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelStart.setVisible(false);
				panelClient.setVisible(true);
			}
		});
		btnClient.setBounds(0, 117, 557, 55);
		panelStart.add(btnClient);
		
		JButton btnProduct = new JButton("Accounts");
		btnProduct.setFont(new Font("Tahoma", Font.PLAIN, 22));
		btnProduct.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelStart.setVisible(false);
				panelProd.setVisible(true);
			}
		});
		btnProduct.setBounds(0, 185, 557, 55);
		panelStart.add(btnProduct);
		
		JLabel lblBank = new JLabel("Bank");
		lblBank.setHorizontalAlignment(SwingConstants.CENTER);
		lblBank.setFont(new Font("Tahoma", Font.PLAIN, 22));
		lblBank.setBounds(151, 63, 222, 41);
		panelStart.add(lblBank);
		
		JButton btnSaveData = new JButton("Save Data");
		btnSaveData.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				try
				{
					FileOutputStream fileOut=new FileOutputStream("E:\\Facultate\\bank.ser");
					ObjectOutputStream streamOut=new ObjectOutputStream(fileOut);
					streamOut.reset();
					streamOut.writeObject(b.persons);
					streamOut.writeObject(b.accounts);
					streamOut.close();
					fileOut.close();
					
					
					
					
				}
				catch(IOException exe)
				{
					exe.printStackTrace();
				}
			}
		});
		btnSaveData.setFont(new Font("Tahoma", Font.PLAIN, 22));
		btnSaveData.setBounds(0, 253, 557, 55);
		panelStart.add(btnSaveData);
		
		JRadioButton rdbtnSavingaccount = new JRadioButton("SavingAccount");
		rdbtnSavingaccount.setBounds(8, 327, 127, 25);
		panelProd.add(rdbtnSavingaccount);
		
		JRadioButton rdbtnSpendingaccount = new JRadioButton("SpendingAccount");
		rdbtnSpendingaccount.setBounds(132, 327, 127, 25);
		panelProd.add(rdbtnSpendingaccount);
		
		ButtonGroup buttonGroup = new ButtonGroup();
		buttonGroup.add(rdbtnSavingaccount);
		buttonGroup.add(rdbtnSpendingaccount);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setBounds(68, 288, 116, 22);
		for(Person p:b.persons)
		{
			comboBox.addItem(p.getName());
		}
		panelProd.add(comboBox);
		
		
		
		textField = new JTextField();
		textField.setBounds(67, 196, 116, 22);
		panelClient.add(textField);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setBounds(67, 228, 116, 22);
		panelClient.add(textField_1);
		textField_1.setColumns(10);
		
		textField_2 = new JTextField();
		textField_2.setBounds(67, 254, 116, 22);
		panelClient.add(textField_2);
		textField_2.setColumns(10);
		
		textField_3 = new JTextField();
		textField_3.setBounds(67, 283, 116, 22);
		panelClient.add(textField_3);
		textField_3.setColumns(10);
		
		JButton btnReturn = new JButton("Return");
		btnReturn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelStart.setVisible(true);
				panelClient.setVisible(false);
				
			}
		});
		
		btnReturn.setBounds(448, 314, 97, 25);
		panelClient.add(btnReturn);
		
		table=new JTable();
		table.setBounds(12, 13, 533, 173);
		panelClient.add(table);
       
        
        JButton btnInsert = new JButton("Add");
        btnInsert.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent arg0) {
        	try{	
        		int id=Integer.parseInt(textField.getText());
        		String name=textField_1.getText();
        		int age=Integer.parseInt(textField_2.getText());
        		String add=textField_3.getText();
        		Person p=new Person(id,name,age,add);
        		b.addPerson(p);
        		comboBox.addItem(p.getName());
        		
        	}
        	
        	catch(NumberFormatException e)
        	{
        		JOptionPane.showMessageDialog(null, "Input is incorect");
        	}
        		
        		
        	}
        });
        
        btnInsert.setBounds(208, 303, 97, 25);
        panelClient.add(btnInsert);
        
        JButton btnDelete = new JButton("Delete");
        btnDelete.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		int id=(int)table.getValueAt(table.getSelectedRow(),0);
        		Person p=b.findById(id); 
        		b.removePerson(p);
        		comboBox.removeItem(p.getName());
        		
        	}
        });
       
        btnDelete.setBounds(208, 227, 97, 25);
        panelClient.add(btnDelete);
        
        JButton btnUpdate = new JButton("Update");
        btnUpdate.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		try{
        		int id=Integer.parseInt(textField.getText());
        		String name=textField_1.getText();
        		int age=Integer.parseInt(textField_2.getText());
        		String add=textField_3.getText();
        		Person p=b.findById(id);
        		p.setAddress(add);
        		p.setAge(age);
        		p.setName(name);
        		}
        		catch(NumberFormatException ex)
            	{
            		JOptionPane.showMessageDialog(null, "Input is incorect");
            	}
        	}
        });
       
        btnUpdate.setBounds(208, 265, 97, 25);
        panelClient.add(btnUpdate);
        
        JButton btnShowClients = new JButton("List");
        btnShowClients.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		
        		DefaultTableModel model = new DefaultTableModel();
                Object[] columnsName = new Object[4];
                columnsName[0] = (Object) new String("PersonID");
                columnsName[1] = (Object) new String("Name");
                columnsName[2] = (Object) new String("Age");
                columnsName[3] = (Object) new String("address");
                model.setColumnIdentifiers(columnsName);
                Object[] rowData = new Object[4];
                     for(int i = 0; i < b.persons.size(); i++){
                	 rowData[0] = b.persons.get(i).getId();
                	 rowData[1] = b.persons.get(i).getName();
                	 rowData[2] = b.persons.get(i).getAge();
                	 rowData[3] = b.persons.get(i).getAddress();
                	model.addRow(rowData);
                	 
                }
                table.setModel(model);
                
        	}
        });
        
        
     
        btnShowClients.setBounds(208, 195, 97, 25);
        panelClient.add(btnShowClients);
        
        JLabel lblId_2 = new JLabel("ID");
        lblId_2.setBounds(0, 202, 56, 16);
        panelClient.add(lblId_2);
        
        JLabel lblName = new JLabel("Name");
        lblName.setBounds(0, 231, 56, 16);
        panelClient.add(lblName);
        
        JLabel lblAge = new JLabel("Age");
        lblAge.setBounds(0, 257, 56, 16);
        panelClient.add(lblAge);
        
        JLabel lblAddress = new JLabel("Address");
        lblAddress.setBounds(0, 286, 56, 16);
        panelClient.add(lblAddress);
		
		textField_4 = new JTextField();
		textField_4.setBounds(68, 242, 116, 22);
		panelProd.add(textField_4);
		textField_4.setColumns(10);
		
		JButton btnReturn_1 = new JButton("Return");
		btnReturn_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelProd.setVisible(false);
				panelStart.setVisible(true);
			}
		});
		
		btnReturn_1.setBounds(439, 327, 97, 25);
		panelProd.add(btnReturn_1);
		
		JButton btnInsertProd = new JButton("Add");
		btnInsertProd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String name=(String) comboBox.getSelectedItem();
				Person p=b.findByName(name);
				int id=Integer.parseInt(textField_7.getText());
				double value=Double.parseDouble(textField_4.getText());
				if (rdbtnSavingaccount.isSelected())
				{
					SavingAccount a=new SavingAccount(id,value,new Date());
					b.addAccount(a, p);
					System.out.println("Saving Account  "+a.getValue());
				}
				else
				{
					SpendingAccount a=new SpendingAccount(id,value,new Date());
					b.addAccount(a, p);
					System.out.println("Spending Account  "+a.getValue());
				}
				
			}
		});
		
		btnInsertProd.setBounds(226, 208, 97, 25);
		panelProd.add(btnInsertProd);
		
		
		
		
		
		textField_7 = new JTextField();
		textField_7.setBounds(68, 209, 116, 22);
		panelProd.add(textField_7);
		textField_7.setColumns(10);
		
		JLabel lblId_1 = new JLabel("Id");
		lblId_1.setBounds(10, 212, 56, 16);
		panelProd.add(lblId_1);
		
		table_1 = new JTable();
		table_1.setBounds(0, 13, 557, 177);
		panelProd.add(table_1);
		
		JButton btnShowProducts = new JButton("List");
		btnShowProducts.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String name=(String) comboBox.getSelectedItem();
				Person p=b.findByName(name);
				ArrayList<Account> accs=b.accounts.get(p);
				DefaultTableModel model = new DefaultTableModel();
                Object[] columnsName = new Object[4];
                columnsName[0] = (Object) new String("ID");
                columnsName[1] = (Object) new String("Value");
                columnsName[2] = (Object) new String("Date");
                columnsName[3] = (Object) new String("Type");
                model.setColumnIdentifiers(columnsName);
                Object[] rowData = new Object[4];
                     for(int i = 0; i < accs.size(); i++){
                	 rowData[0] = accs.get(i).getId();
                	 rowData[1] = accs.get(i).getValue();
                	 DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                	 rowData[2]=dateFormat.format(accs.get(i).getDate());
                	 rowData[3]=accs.get(i).getClass().getName().substring(5);
                	  model.addRow(rowData);
                     }
                     table_1.setModel(model);
			}
		});
		
		btnShowProducts.setBounds(226, 287, 97, 25);
		panelProd.add(btnShowProducts);
		
		JButton btnDeleteProd = new JButton("Remove");
		btnDeleteProd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int id=(int)table_1.getValueAt(table_1.getSelectedRow(),0);
				String name=(String) comboBox.getSelectedItem();
				Person p=b.findByName(name);
				Account a=b.findAccount(p, id);
				b.removeAccount(a, p);
				
				
				
				
			}
		});
		
		btnDeleteProd.setBounds(226, 241, 97, 25);
		panelProd.add(btnDeleteProd);
		
		JLabel lblValue = new JLabel("Value");
		lblValue.setBounds(10, 245, 56, 16);
		panelProd.add(lblValue);
		
		
		JLabel lblHolder = new JLabel("Holder");
		lblHolder.setBounds(10, 291, 56, 16);
		panelProd.add(lblHolder);
		
		textField_5 = new JTextField();
		textField_5.setBounds(441, 242, 116, 22);
		panelProd.add(textField_5);
		textField_5.setColumns(10);
		
		JButton btnStore = new JButton("Store");
		btnStore.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
				double sum=Double.parseDouble(textField_5.getText());
				int id=(int)table_1.getValueAt(table_1.getSelectedRow(),0);
				String name=(String) comboBox.getSelectedItem();
				Person p=b.findByName(name);
				Account a=b.findAccount(p, id);
				a.storeMoney(sum);
				}
				catch(ArrayIndexOutOfBoundsException exe)
				{
					JOptionPane.showMessageDialog(null, "Please select an account");
				}
				catch(NumberFormatException exeption)
				{
					JOptionPane.showMessageDialog(null, "Invalid sum");
				}
			}
		});
		btnStore.setBounds(351, 287, 97, 25);
		panelProd.add(btnStore);
		
		JButton btnWithdraw = new JButton("Withdraw");
		btnWithdraw.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try{
				double sum=Double.parseDouble(textField_5.getText());
				int id=(int)table_1.getValueAt(table_1.getSelectedRow(),0);
				String name=(String) comboBox.getSelectedItem();
				Person p=b.findByName(name);
				Account a=b.findAccount(p, id);
				a.withdrawMoney(sum);
				}
				catch(ArrayIndexOutOfBoundsException exe)
				{
					JOptionPane.showMessageDialog(null, "Please select an account");
				}
				catch(NumberFormatException exeption)
				{
					JOptionPane.showMessageDialog(null, "Invalid sum");
				}
			}
		});
		btnWithdraw.setBounds(460, 287, 97, 25);
		panelProd.add(btnWithdraw);
		
		JLabel lblSum =  new JLabel("Sum");
		lblSum.setBounds(373, 245, 56, 16);
		panelProd.add(lblSum);
		
		
		
	
}
}

