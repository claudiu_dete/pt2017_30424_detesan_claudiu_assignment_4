package bank;

import java.util.ArrayList;
import java.util.Date;


import junit.framework.TestCase;

public class BankTest extends TestCase {
	
	public void testPersonIntroduction()
	{
		Bank b=new Bank();
		Person p1=new Person(1,"Detesan",21,"Marasti");
		Person p2=new Person(2,"Dete",22,"Observator");
		b.addPerson(p1);
		b.addPerson(p2);
		assertEquals(2,b.persons.size());
		
	}
	
	public void testWithdraw()
	{
		SpendingAccount a=new SpendingAccount(1,2500,new Date());
	    a.withdrawMoney(1000);
	    assertEquals(1500.00,a.getValue());
	}
	
	public void testWithdraw1()
	{
		SavingAccount a=new SavingAccount(1,2500,new Date());
		a.withdrawMoney(1000);
		assertEquals(0.00,a.getValue());
		
	}
	
	public void testStoreMoney()
	{
		SpendingAccount a=new SpendingAccount(1,2500,new Date());
		a.storeMoney(1000);
		a.storeMoney(2500);
		assertEquals(6000.0,a.getValue());
		
	}
	
	public void testStoreMoney2()
	{
	  SavingAccount a=new SavingAccount(1,2500,new Date());
	  a.storeMoney(1500);
	  a.storeMoney(1500);
	  a.storeMoney(5000);
	  assertEquals(4125.0,a.getValue());
	}
	
	public void testRemovePerson()
	{
		Bank b=new Bank();
		Person p1=new Person(1,"Detesan",21,"Marasti");
		Person p2=new Person(2,"Dete",22,"Observator");
		b.addPerson(p1);
		b.addPerson(p2);
		int size=b.persons.size();
		b.removePerson(p1);
		assertEquals(size,b.persons.size()+1);
		
	}
	
	public void testNbAccounts()
	{
		Bank b=new Bank();
		Person p1=new Person(1,"Detesan",21,"Marasti");
		b.addPerson(p1);
		SavingAccount a=new SavingAccount(1,1000,new Date());
		SpendingAccount a1=new SpendingAccount(2,1000,new Date());
		b.addAccount(a,p1);
		b.addAccount(a1,p1);
		ArrayList<Account> accs=b.accounts.get(p1);
		
		assertEquals(accs.size(),2);
		
	}
	
	public void testNbAccounts1()
	{
		Bank b=new Bank();
		Person p1=new Person(1,"detesan",21,"Marasti");
		b.addPerson(p1);
		SavingAccount a=new SavingAccount(1,1000,new Date());
		SpendingAccount a1=new SpendingAccount(2,1000,new Date());
		b.addAccount(a,p1);
		b.addAccount(a1,p1);
		b.removeAccount(a1,p1);
		ArrayList<Account> accs=b.accounts.get(p1);
		
		assertEquals(accs.size(),1);
	}
	
	
	


	

}
