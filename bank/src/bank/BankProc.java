package bank;

public interface BankProc {
	//mutuators
	/**
	 * adds a new person to the List
	 * @pre p!=NULL 
	 * @pre p.exists==false
	 * @post size()==size()@pre+1 && p.exists()==false
	 */
	public void addPerson(Person p);
	
	/**
	 * removes a person from the List
	 * @pre p!=NULL
	 * @pre p.exists==true
	 * @post size()=size()@pre +1
	 */
	public void removePerson(Person p);
	
	/**
	 * adds a new account to a person
	 * @pre holder!=NULL
	 * @pre account!=NULL
	 * @post accountList.size()=accountList.size()@pre+1
	 */
	 public void addAccount(Account a,Person holder);
	 /**
	  * removes an account of a specified holder
	  * @pre holder!=null
	  * @pre account.exists()==true
	  * @post account.size()=accounts.size()@pre-1
	  */
	 
	 public void removeAccount(Account a,Person holder);
	 /**
	  * finds an account corresponding to a person
	  * @pre holder !=null
	  * 
	  */
	 
	 public Account findAccount(Person holder,int id);
	 /**
	  * 
	  * finds a person by his/her id
	  * @pre id>0 (id is valid)
	  */
	 
	 public Person findById(int id);
	 /**
	  * 
	  *finds a person by name
	  * @pre name!=null
	  */
	 
	 public Person findByName(String name);
	 /**
	  * 
	  * class invariant
	  */
	 public boolean isWellFormed();
	
		
	

}
