package bank;

import java.io.Serializable;
import java.util.Date;
import java.util.Observable;

public class Account extends Observable implements Serializable{

	private int id;
	protected double value;
	private Date creationDate;
	
	public Account(int id,double value,Date d)
	{
		this.id=id;
		this.value=value;
		this.creationDate=d;
	}
	
	public Account()
	{
		
	}
	
	public int getId()
	{
		return this.id;
	}
	
	public void setId(int id)
	{
		this.id=id;
	}
	
	public double getValue()
	{
		return this.value;
	}
	
	public void setValue(double v)
	{
		this.value=v;
	}
	
	public Date getDate()
	{
		return this.creationDate;
	}
	
	
	
	public void withdrawMoney(double sum)
	{
		this.value=this.value-sum;
		
	}
	
	public void storeMoney(double sum)
	{
		this.value=this.value+sum;
	}
}
