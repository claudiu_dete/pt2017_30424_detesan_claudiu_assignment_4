package bank;

import java.util.Date;

public class SpendingAccount extends Account {
	
	public SpendingAccount(int id,double value, Date d)
	{
		super(id,value,d);
	}
	
	public void withdrawMoney(double  sum)
	{
		if(value>sum)
		{
			setChanged();
			notifyObservers("The sum "+sum+" has been taken from account id="+this.getId());
			value=value-sum;
		}
	}
	
	public void storeMoney(double sum)
	{
		setChanged();
		notifyObservers("The sum "+sum+" has been stored in account id="+this.getId());
	    value=value+sum;	
	}
	
	

}
