package bank;

import java.io.Serializable;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JOptionPane;

public class Person implements Serializable,Observer {
	
	
	private int id;
	private String name;
	private int age;
	private String address;
	private String change;
	
	public Person(int id,String name,int age,String address)
	{
		this.id=id;
		this.name=name;
		this.age=age;
		this.address=address;
	}
	
	public Person()
	{
		
	}
	
	public int getId()
	{
		return this.id;
	}
	
	public void setId(int id)
	{
		this.id=id;
	}
	
	public String getName()
	{
		return this.name;
	}
	
	public void setName(String name)
	{
		this.name=name;
	}
	
	public int getAge()
	{
		return this.age;
	}
	
	public void setAge(int a)
	{
		this.age=a;
	}
	
	public String getAddress()
	{
		return this.address;
	}
	
	public void setAddress(String a)
	{
		this.address=a;
	}
	
	public int hashCode()
	{
		return this.id;
	}
	
	public boolean equals(Person p)
	{
		if(this.id==p.getId()) return true;
		return false;
	}
	
	public String toString()
	{
		return this.name+" "+this.address;
	}
	
	public void update(Observable o,Object obj)
	{
		 if (obj instanceof String)
		 {
			 System.out.println("Person "+this.getId()+" has been notified of the account change");
			 this.change=obj.toString();
			 JOptionPane.showMessageDialog(null, obj.toString());
		 }
	}


}
