package bank;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class Bank implements BankProc {
	
	List<Person> persons=new ArrayList<Person>();
	HashMap<Person,ArrayList<Account>> accounts=new HashMap<Person, ArrayList<Account>>();
	
	public Bank()
	{
		
	}
	
	
	public boolean isWellFormed()
	{
		if(persons==null || accounts==null)
		{
			return false;
		}
		if(persons.size()!=accounts.size())
		{
			return false;
		}
		
		return true;
	}
	
	public void addPerson(Person p)
	{   assert p!=null :"Person which is added is null";
	    assert findById(p.getId())==null : "Person already exists";
	    assert isWellFormed();
	    int size=persons.size();
		persons.add(p);
		ArrayList<Account> accs=new ArrayList<Account>();
		accounts.put(p, accs);
		
		assert persons.size()==size+1 :"Person was not added";
		
	}
	
	public Person findById(int id)
	{
		assert id !=0 :"id does not exist";
		assert isWellFormed();
		Iterator<Person> it=persons.iterator();
		Person p=new Person();
		while(it.hasNext()){
			p= it.next();
			if(p.getId()==id)
			{
				return p;
			}
		}
		return null;
	}
	
	public Person findByName(String name)
	{
		assert name!=null :"Name is null";
		assert isWellFormed();
		Iterator<Person> it=persons.iterator();
		Person p=new Person();
		while(it.hasNext()){
			p= it.next();
			if(p.getName()==name)
			{
				return p;
			}
		}
		return null;
	}
	
	public void removePerson(Person p)
	{
		assert p!=null :"Person which is removed is null";
		assert isWellFormed();
		assert findById(p.getId())!=null : "Person does not exist";
		int size=persons.size();
		persons.remove(p);
		accounts.remove(p);
		assert persons.size()==size-1 :"Person was not removed";
	}
	
	public void addAccount(Account a,Person holder)
	{
		assert holder!=null :"Holder is null";
		assert a!=null: "Account is null";
		assert isWellFormed();
		ArrayList<Account> accs=accounts.get(holder);
		int size=0;
		if(accs!=null) size=accs.size();
		accs.add(a);
		a.addObserver(holder);
	    
		assert accs.size()==size+1 :"Account was not added";
		
	}
	
	public Account findAccount(Person holder,int id)
	{
		 assert holder!=null :"Holder is null";
		 
		 ArrayList<Account> accs=accounts.get(holder);
		 Iterator<Account> it=accs.iterator();
		 Account a=new Account();
		 while(it.hasNext())
		 {
			 a=it.next();
			 if(a.getId()==id) return a;
		 }
		 
		 return null;
	}
	
	public void removeAccount(Account a,Person holder)
	{
		assert holder!=null :"Holder is null";
		assert a!=null :"Account is null";
		assert isWellFormed();
		ArrayList<Account> accs=accounts.get(holder);
		int size=accs.size();
		accs.remove(a);
		assert accs.size()==size-1:"Account was not deleted";
	}
	
	

			
				
		
	
	
	
	
	
	
	

}
